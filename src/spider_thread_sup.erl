-module(spider_thread_sup).
-behavior(supervisor).

-export([
  start_link/0,
  start_child/2
]).
-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).
start_child(Who, Interval) ->
  supervisor:start_child(?SERVER, [Who, Interval]).

init([]) ->
  Session  = {spider_thread, {spider_thread, start_link, []},
              temporary, brutal_kill, worker, [spider_thread]},
  Children = [Session],
  RestartStrategy = {simple_one_for_one, 0, 1},
  {ok, {RestartStrategy, Children}}.
