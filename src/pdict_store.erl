-module(pdict_store).
-include("cloap_record.hrl").
-export([
         userinfo_get/1, userinfo_get/2,
         userinfo_put/2, userinfo_put/3
        ]).

% ------------------------------------------------
% 在进程字典中查找用户信息
% ------------------------------------------------
userinfo_get(Who) ->
    case cloap_store:find(user_process, Who) of
        [H|_T] -> H;
        _ -> not_found
    end.

userinfo_get(Who, pid) ->
    case userinfo_get(Who) of
        not_found ->
            not_found;
        Rec ->
            case erlang:process_info(Rec#user_process.pid) of
                undefined -> not_found;
                _ -> Rec#user_process.pid
            end
    end.

  
userinfo_put(Who, _Key) ->
    case userinfo_get(Who) of
        not_found -> ok;
        Rec ->
            cloap_store:delete(Rec)
    end.


userinfo_put(Who, _Key, Value) ->
    case userinfo_get(Who) of
        not_found ->
            cloap_store:insert(#user_process{login = Who, pid = Value});
        Rec ->
            cloap_store:update(Rec#user_process{pid = Value})
    end.
