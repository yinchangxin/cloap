-module(cloap_acl).
-export([allow_apps/1]).
-include("cloap_record.hrl").

allow_apps(User) ->
    Acls = cloap_store:find_all(acl),
    
    Fun = fun(X) ->
                  case X#acl.default of
                      allow ->
                          case lists:member(User, X#acl.deny) of
                              true -> [];
                              false -> [X#acl.appid]
                          end;
                      deny ->
                          case lists:member(User, X#acl.allow) of
                              true -> [X#acl.appid];
                              false -> []
                          end
                  end
          end,
    lists:flatten([Fun(Rec) || Rec <- Acls]).
